﻿using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace CustomRender.Behavior
{
    public class DigitBehavior : Behavior<Entry>
    {
        public static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(DigitBehavior), false);
        private const string digitRegex = @"^[0-9]+$";
        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;
        public bool IsValid
        {
            get
            {
                return (bool)this.GetValue(IsValidProperty);
            }
            set
            {
                this.SetValue(IsValidPropertyKey, value);
            }
        }
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += bindable_TextChanged;

        }

        private void bindable_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry;
            //bool isValid;
            entry = (Entry)sender;
            this.IsValid = Regex.IsMatch(e.NewTextValue, digitRegex);
            entry.TextColor = this.IsValid ? Color.Default : Color.Red;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= this.bindable_TextChanged;
        }
    }
}
